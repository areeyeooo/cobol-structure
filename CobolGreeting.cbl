      ******************************************************************
      * Author:
      * Date:
      * Purpose:
      * Tectonics: cobc
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID. CobolGreeting.
       *>Program to display Cobol greetings
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 ItemNum  PIC 9 VALUE 5.

       PROCEDURE DIVISION.
       BeginProgram.
            PERFORM DisplayGreeting ItemNum TIMES.
            STOP RUN.

       DisplayGreeting.
           DISPLAY "Greeting from COBOL".
